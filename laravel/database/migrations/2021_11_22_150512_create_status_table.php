<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateStatusTable extends Migration
{
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('capitulos_posts')->onDelete('cascade');
            $table->boolean('iniciado')->default(false);
            $table->boolean('concluido')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('status');
    }
}
