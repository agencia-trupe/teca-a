<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCapitulosTable extends Migration
{
    public function up()
    {
        Schema::create('capitulos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('cor');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('capitulos');
    }
}
