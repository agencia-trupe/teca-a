<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCapitulosPostsTable extends Migration
{
    public function up()
    {
        Schema::create('capitulos_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('capitulo_id')->unsigned();
            $table->foreign('capitulo_id')->references('id')->on('capitulos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_post');
            $table->longText('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('capitulos_posts');
    }
}
