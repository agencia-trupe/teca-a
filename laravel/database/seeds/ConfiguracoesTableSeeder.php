<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title'                      => 'TECA A',
            'description'                => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics_ua'               => '',
            'analytics_gg'               => '',
            'pixel_facebook'             => '',
        ]);
    }
}
