@extends('frontend.common.template')

@section('content')

@include('frontend.common.header')

<section class="politica center">

    <article class="faixa">
        <a href="{{ route('sumario') }}" class="teca">
            <img src="{{ asset('assets/img/layout/icone-tecaa.png') }}" alt="TECA" class="img-teca">
        </a>
        <a href="{{ route('sumario') }}" class="livro">
            <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="TECA" class="img-livro">
        </a>
    </article>

    <article class="conteudo">
        <h2 class="titulo">POLÍTICA DE PRIVACIDADE</h2>

        <div class="texto">{!! $politica->texto !!}</div>
    </article>

</section>

@endsection