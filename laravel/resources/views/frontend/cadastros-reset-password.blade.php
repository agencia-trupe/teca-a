@extends('frontend.common.template')

@section('content')

<section class="cadastros-reset">

    <div class="center">

        <div class="login redefinir">
            <h2 class="titulo">REDEFINIR SENHA</h2>
            <form action="{{ route('reset.post') }}" method="POST" class="form-reset">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                <label for="email">e-mail</label>
                <input type="email" name="email" value="{{ $email or old('email') }}" required>
                <label for="password">senha</label>
                <input type="password" name="password" value="{{ old('password') }}" required>
                <label for="password_confirm">confirmar senha</label>
                <input type="password" id="password-confirm" name="password_confirmation" required>

                <button type="submit" class="btn-redefinir">REDEFINIR</button>
            </form>
            <a href="{{ route('cadastros.login') }}" class="link-login">« voltar para o login</a>

            @if(session('success'))
            <div class="flash flash-success">
                {!! session('success') !!}
            </div>
            @endif

            @if(session('error'))
            <div class="flash flash-error">
                {!! session('error') !!}
            </div>
            @endif

            <!-- position absolute -->
            <a href="{{ route('cadastros.create') }}" class="link-cadastro">» NÃO TENHO CADASTRO: Iniciar</a>
        </div>

        <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="SBC" class="capa-tecaa">

        <div class="logo-sbc">
            <img src="{{ asset('assets/img/layout/marca-sbc.svg') }}" alt="SBC" class="img-sbc">
        </div>

    </div>

</section>

@endsection