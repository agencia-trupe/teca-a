@extends('frontend.common.template')

@section('content')

@include('frontend.common.header')

<section class="paginas center">

    <article class="faixa">
        <a href="{{ route('sumario') }}" class="teca" style="background-color: {{$postTexto->cor}}; filter: brightness(0.9) grayscale(0.3);">
            <img src="{{ asset('assets/img/layout/icone-tecaa.png') }}" alt="TECA" class="img-teca">
        </a>
        <div class="livro" style="background-color: {{$postTexto->cor}};">
            <div class="capitulo">{{ $postTexto->cap_titulo }}</div>
            <a href="{{ route('sumario') }}" class="livro">
                <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="TECA" class="img-livro">
            </a>
        </div>
    </article>

    <article class="conteudo">
        <h2 class="titulo">{{ $postTexto->titulo_post }}</h2>
        <div class="texto">{!! $postTexto->texto !!}</div>

        <div class="links-conteudo">
            @if(!empty($status) && $status->concluido == 1)
            <a href="{{ route('post.concluido', ['cadastro' => auth('cadastros')->user()->id, 'post' => $postTexto->id]) }}" class="link-conteudo-lido lido">
                CONTEÚDO LIDO
            </a>
            @else
            <a href="{{ route('post.concluido', ['cadastro' => auth('cadastros')->user()->id, 'post' => $postTexto->id]) }}" class="link-conteudo-lido">
                CONTEÚDO LIDO
            </a>
            @endif
            <a href="{{ route('post', [$postTexto->cap_slug, $postTexto->slug, 'proximo']) }}" class="link-proximo-post">AVANÇAR PARA O PRÓXIMO TEXTO
            </a>
        </div>
        <a href="{{ route('sumario') }}" class="link-sumario">« retornar para o SUMÁRIO</a>
    </article>

</section>

<style type="text/css">
    h3 {
        background-color: {{$postTexto->cor}}7F;
    }

    h2 {
        color: {{$postTexto->cor}};
    }
</style>

@endsection