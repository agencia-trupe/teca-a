@extends('frontend.common.template')

@section('content')

@include('frontend.common.header')

<section class="sumario center">

    <article class="faixa">
        <a href="{{ route('sumario') }}" class="teca">
            <img src="{{ asset('assets/img/layout/icone-tecaa.png') }}" alt="TECA" class="img-teca">
        </a>
        <a href="{{ route('sumario') }}" class="livro">
            <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="TECA" class="img-livro">
        </a>
    </article>

    <article class="conteudo">
        <h2 class="titulo">SUMÁRIO</h2>

        @foreach($capitulos as $capitulo)
        <div class="capitulo" id="{{$capitulo->id}}" style="background-color: {{$capitulo->cor}};">
            <div class="titulo-capitulo">
                {{ $capitulo->titulo }}
            </div>
            <div class="posts">
                @php
                $totalPosts = [];
                foreach($posts as $post) {
                if($post->capitulo_id == $capitulo->id) {
                $totalPosts[] = $post;
                }
                }
                @endphp

                @if(count($totalPosts) == 1)

                @foreach($posts as $post)
                @if($post->capitulo_id == $capitulo->id)
                <div class="post unico">
                    <a href="{{ route('post', [$capitulo->slug, $post->slug]) }}" class="link-post">{{ $post->titulo_post }}</a>

                    <div class="status">

                        @php
                        $statusPost = [];
                        foreach($status as $s) {
                        if($s->post_id == $post->id) {
                        $statusPost[] = $s;
                        }
                        }
                        @endphp

                        @forelse($statusPost as $s)
                        @if($s->concluido == 1)
                        <div class="concluido @if($s->concluido == 1) active @endif" style="background-color: {{$capitulo->cor}}; color: #FFF; border:1px solid {{$capitulo->cor}};">CONCLUÍDO</div>
                        @elseif($s->iniciado == 1)
                        <div class="iniciado @if($s->iniciado == 1) active @endif" style="background-color: {{$capitulo->cor}}99; color: #FFF; border:1px solid {{$capitulo->cor}}99;">INICIADO</div>
                        @endif
                        @break

                        @empty
                        <div class="nenhum" style="background-color: #FFF; color: {{$capitulo->cor}}; border:1px solid {{$capitulo->cor}};">NÃO INICIADO</div>
                        @endforelse
                    </div>

                </div>
                @endif
                @endforeach

                @else

                @foreach($posts as $post)
                @if($post->capitulo_id == $capitulo->id)
                <div class="post">
                    <a href="{{ route('post', [$capitulo->slug, $post->slug]) }}" class="link-post">{{ $post->titulo_post }}</a>

                    <div class="status">

                        @php
                        $statusPost = [];
                        foreach($status as $s) {
                        if($s->post_id == $post->id) {
                        $statusPost[] = $s;
                        }
                        }
                        @endphp

                        @forelse($statusPost as $s)
                        @if($s->concluido == 1)
                        <div class="concluido @if($s->concluido == 1) active @endif" style="background-color: {{$capitulo->cor}}; color: #FFF; border:1px solid {{$capitulo->cor}};">CONCLUÍDO</div>
                        @elseif($s->iniciado == 1)
                        <div class="iniciado @if($s->iniciado == 1) active @endif" style="background-color: {{$capitulo->cor}}99; color: #FFF; border:1px solid {{$capitulo->cor}}99;">INICIADO</div>
                        @endif
                        @break

                        @empty
                        <div class="nenhum" style="background-color: #FFF; color: {{$capitulo->cor}}; border:1px solid {{$capitulo->cor}};">NÃO INICIADO</div>
                        @endforelse
                    </div>

                </div>
                @endif
                @endforeach

                @endif
            </div>
        </div>
        @endforeach
    </article>

</section>

@endsection