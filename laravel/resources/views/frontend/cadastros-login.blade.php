@extends('frontend.common.template')

@section('content')

<section class="cadastros-login">

    <div class="center">

        <div class="login">
            <h2 class="titulo">LOGIN</h2>
            <form action="{{ route('cadastros.login') }}" method="POST" class="form-login">
                {!! csrf_field() !!}
                <label for="email">e-mail</label>
                <input type="email" name="email" value="{{ old('email') }}" required>
                <label for="senha">senha</label>
                <input type="password" name="senha" value="{{ old('senha') }}" required>
                <button type="submit" class="btn-entrar">ENTRAR</button>
            </form>
            <a href="{{ route('cadastros.reset-request') }}" class="link-esqueci">» esqueci minha senha</a>

            @if(session('success'))
            <div class="flash flash-success">
                {!! session('success') !!}
            </div>
            @endif

            @if(session('error'))
            <div class="flash flash-error">
                {!! session('error') !!}
            </div>
            @endif

            <!-- position absolute -->
            <a href="{{ route('cadastros.create') }}" class="link-cadastro">» NÃO TENHO CADASTRO: Iniciar</a>
        </div>

        <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="SBC" class="capa-tecaa">

        <div class="logo-sbc">
            <img src="{{ asset('assets/img/layout/marca-sbc.svg') }}" alt="SBC" class="img-sbc">
        </div>

    </div>

</section>

@endsection