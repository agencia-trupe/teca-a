@extends('frontend.common.template')

@section('content')

<section class="cadastros-reset-request">

    <div class="center">

        <div class="login">
            <h2 class="titulo esqueci">ESQUECI MINHA SENHA</h2>
            <form action="{{ route('reset-request.post') }}" method="POST" class="form-esqueci">
                {!! csrf_field() !!}
                <label for="email">e-mail</label>
                <input type="email" name="email" value="{{ old('email') }}" required>
                <button type="submit" class="btn-solicitar">SOLICITAR</button>
            </form>
            <a href="{{ route('cadastros.login') }}" class="link-login">« voltar para o login</a>

            @if(session('success'))
            <div class="flash flash-success">
                {!! session('success') !!}
            </div>
            @endif

            @if(session('error'))
            <div class="flash flash-error">
                {!! session('error') !!}
            </div>
            @endif

            <!-- position absolute -->
            <a href="{{ route('cadastros.create') }}" class="link-cadastro">» NÃO TENHO CADASTRO: Iniciar</a>
        </div>

        <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="SBC" class="capa-tecaa">

        <div class="logo-sbc">
            <img src="{{ asset('assets/img/layout/marca-sbc.svg') }}" alt="SBC" class="img-sbc">
        </div>

    </div>

</section>

@endsection