@extends('frontend.common.template')

@section('content')

@include('frontend.common.header')

<section class="conteudo-restrito center">

    <article class="faixa">
        <a href="{{ route('sumario') }}" class="teca">
            <img src="{{ asset('assets/img/layout/icone-tecaa.png') }}" alt="TECA" class="img-teca">
        </a>
        <a href="{{ route('sumario') }}" class="livro">
            <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="TECA" class="img-livro">
        </a>
    </article>

    <article class="conteudo">
        <h2 class="titulo">Olá,</h2>

        <div class="texto">
            <p class="frase">O acesso a este conteúdo é restrito.</p>
            <p class="frase">Você deve ter cadastro prévio aprovado. Entre em contato com a SBC - Sociedade Brasileira de Cardiologia.</p>
        </div>
    </article>

</section>

@endsection