@extends('frontend.common.template')

@section('content')

@include('frontend.common.header')

<section class="cadastros-create center">

    <article class="faixa">
        <a href="{{ route('sumario') }}" class="teca">
            <img src="{{ asset('assets/img/layout/icone-tecaa.png') }}" alt="TECA" class="img-teca">
        </a>
        <a href="{{ route('sumario') }}" class="livro">
            <img src="{{ asset('assets/img/layout/capa-tecaa.jpg') }}" alt="TECA" class="img-livro">
        </a>
    </article>

    <article class="conteudo">
        <h2 class="titulo">ATUALIZE SEU CADASTRO</h2>

        <form action="{{ route('update.post', $user->id) }}" method="POST" class="form-cadastro">
            {!! csrf_field() !!}

            <div class="input-group">
                <label for="nome">nome completo</label>
                <input type="text" name="nome" value="{{ $user->nome }}" required>
            </div>
            <div class="input-group">
                <label for="email">e-mail</label>
                <input type="email" name="email" value="{{ $user->email }}" required>
            </div>
            <div class="input-group">
                <label for="senha">senha</label>
                <input type="password" name="senha" required>
            </div>
            <div class="input-group">
                <label for="senha_confirmation">repetir senha</label>
                <input type="password" name="senha_confirmation" id="senha_confirmation" required>
            </div>

            <button type="submit" class="btn-cadastrar">ATUALIZAR</button>
        </form>

        @if($errors->any())
        <div class="flash flash-error">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

    </article>


</section>

@endsection