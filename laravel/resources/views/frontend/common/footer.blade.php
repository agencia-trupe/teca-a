<footer>

    <div class="center">

        <img src="{{ asset('assets/img/layout/marca-sbc.svg') }}" alt="SBC" class="img-sbc">

        <div class="site-sbc">
            <p class="visite">Visite o site da SBC</p>
            <a href="https://www.portal.cardiol.br/" target="_blank" class="link-sbc">portal.cardiol.br</a>
        </div>

        <div class="menu">
            <a href="{{ route('cadastros.login') }}" class="link-footer link-login">» LOGIN</a>
            <a href="#" class="link-footer link-aviso-de-cookies">» AVISO DE COOKIES</a>
            <a href="{{ route('politica-de-privacidade') }}" class="link-footer">» POLÍTICA DE PRIVACIDADE</a>
        </div>

        <div class="copyright">
            <p class="frase"> © {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
            <p class="frase">Todos os direitos reservados.</p>
        </div>

    </div>

</footer>