<header>

    <div class="center">
        @if(auth('cadastros')->check())
        <a href="#" class="link-logado">Olá {{ auth('cadastros')->user()->nome }}
            <img src="{{ asset('assets/img/layout/seta-cadastro.svg') }}" class="img-seta">
        </a>
        <!-- submenu logado -->
        <div class="submenu-logado" style="display: none;">
            <a href="{{ route('cadastros.update', auth('cadastros')->user()->id) }}" class="link-alterar">ALTERAR SENHA</a>
            <a href="{{ route('cadastros.logout') }}" class="link-sair">SAIR</a>
        </div>
        @else
        <a href="#" class="link-deslogado">Olá! Cadastre-se.
            <img src="{{ asset('assets/img/layout/seta-cadastro.svg') }}" alt="Cadastre-se" class="img-seta">
        </a>
        <!-- submenu deslogado -->
        <div class="submenu-deslogado" style="display: none;">
            <a href="{{ route('cadastros.create') }}" class="link-cadastro">CADASTRE-SE</a>
            <a href="{{ route('cadastros.login') }}" class="link-login">LOGIN</a>
        </div>
        @endif
    </div>

</header>