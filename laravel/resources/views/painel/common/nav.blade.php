<ul class="nav navbar-nav">

    <li class="dropdown @if(Tools::routeIs(['painel.emails*', 'painel.cadastros*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cadastros<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.emails*')) class="active" @endif>
                <a href="{{ route('painel.emails.index') }}">E-mails Liberados</a>
            </li>
            <li @if(Tools::routeIs('painel.cadastros*')) class="active" @endif>
                <a href="{{ route('painel.cadastros.index') }}">Cadastros Ativos</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.capitulos*')) class="active" @endif>
        <a href="{{ route('painel.capitulos.index') }}">Capítulos</a>
    </li>

    <li @if(Tools::routeIs('painel.contatos.index')) class="active" @endif>
        <a href="{{ route('painel.contatos.index') }}">Informações de Contato</a>
    </li>

</ul>