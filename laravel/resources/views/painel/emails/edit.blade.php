@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>E-mails Liberados |</small> Editar E-mail</h2>
</legend>

{!! Form::model($email, [
'route' => ['painel.emails.update', $email->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.emails.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection