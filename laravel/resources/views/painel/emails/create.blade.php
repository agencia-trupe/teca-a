@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>E-mails Liberados |</small> Adicionar E-mail</h2>
</legend>

{!! Form::open(['route' => 'painel.emails.store', 'files' => true]) !!}

@include('painel.emails.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection