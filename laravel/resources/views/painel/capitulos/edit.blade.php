@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Capítulos |</small> Editar Capítulo</h2>
</legend>

{!! Form::model($capitulo, [
'route' => ['painel.capitulos.update', $capitulo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.capitulos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection