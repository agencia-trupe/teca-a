@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_post', 'Título') !!}
    {!! Form::text('titulo_post', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBlog']) !!}

    <p class="obs" style="color:red;margin:10px 0; font-style:italic;">Usar <strong>Título 2</strong> para subtítulos</p>
    <p class="obs" style="color:red;margin:0 0; font-style:italic;">Usar <strong>Título 3</strong> para pontos-chave</p>
    <p class="obs" style="color:red;margin:10px 0; font-style:italic;">Usar <strong>Título 6</strong> para legendas</p>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.capitulos.posts.index', $capitulo->id) }}" class="btn btn-default btn-voltar">Voltar</a>