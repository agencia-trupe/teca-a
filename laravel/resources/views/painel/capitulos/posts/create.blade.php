@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Capítulo: {{ $capitulo->titulo }} |</small> Adicionar Post</h2>
</legend>

{!! Form::model($capitulo, [
'route' => ['painel.capitulos.posts.store', $capitulo->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.capitulos.posts.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection