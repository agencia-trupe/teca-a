@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Capítulo: {{ $capitulo->titulo }} |</small> Editar Post</h2>
</legend>

{!! Form::model($posts, [
'route' => ['painel.capitulos.posts.update', $capitulo->id, $posts->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.capitulos.posts.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection