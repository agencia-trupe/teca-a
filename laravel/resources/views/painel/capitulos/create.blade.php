@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Capítulos |</small> Adicionar Capítulo</h2>
</legend>

{!! Form::open(['route' => 'painel.capitulos.store', 'files' => true]) !!}

@include('painel.capitulos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection