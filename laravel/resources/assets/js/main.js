import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(window).load(function () {
  // heigh faixa
  var heightFaixa = $("section").height();
  $("section .faixa").css("height", heightFaixa);
  // heigh faixa
  var heightSumario = $("section.sumario").height();
  $(".sumario .faixa").css("height", heightSumario);
  // heigh faixa
  var heightPagina = $("section.paginas").height();
  $(".paginas .faixa").css("height", heightPagina);

  // modal marcação inicio de leitura
  $(".modal-inicio-leitura").show();

  // height sumario mobile
  if ($(window).width() < 800) {
    var itensPost = $(".sumario .post");
    itensPost.each(function () {
      var heightPost = $(this).height();
      $(this).children(".status").css("height", heightPost);
    });
  } else {
    // height sumario capitulo 1 post
    var itensPostUnico = $(".sumario .post.unico");
    itensPostUnico.each(function () {
      var heightPostUnico = $(this).parent().parent().height();
      $(this).css("height", heightPostUnico);
    });
  }
});

$(document).ready(function () {
  // submenu header
  if ($(window).width() < 800) {
    $("header .link-logado").click(function (e) {
      e.preventDefault();
      $("header .submenu-logado").css("display", "block").addClass("active");
    });
    $("header .link-deslogado").click(function (e) {
      e.preventDefault();
      $("header .submenu-deslogado").css("display", "block").addClass("active");
    });
  } else {
    $("header .link-logado").mouseenter(function () {
      $("header .submenu-logado").css("display", "block").addClass("active");
    });
    $("header .submenu-logado").mouseleave(function () {
      $(this).css("display", "none").removeClass("active");
    });

    $("header .link-deslogado").mouseenter(function () {
      $("header .submenu-deslogado").css("display", "block").addClass("active");
    });
    $("header .submenu-deslogado").mouseleave(function () {
      $(this).css("display", "none").removeClass("active");
    });
  }

  // cliques leitura concluida
  $(".link-conteudo-lido").click(function (e) {
    e.preventDefault();
    var url = $(this).attr("href");
    ajaxStatusConcluido(url);
  });

  $(".link-proximo-post").click(function (e) {
    e.preventDefault();
    var url = $(this).parent().children(".link-conteudo-lido").attr("href");
    ajaxStatusConcluido(url);
    window.location.href = $(this).attr("href");
  });

  var ajaxStatusConcluido = function (url) {
    $.ajax({
      type: "GET",
      url: url,
      success: function (data, textStatus, jqXHR) {
        console.log(data.status);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  };

  $(".link-proximo-post").mouseenter(function () {
    $(".link-conteudo-lido").addClass("hover-active");
  });
  $(".link-proximo-post").mouseleave(function () {
    $(".link-conteudo-lido").removeClass("hover-active");
  });

  // AVISO DE COOKIES
  $("footer .link-aviso-de-cookies").click(function(e) {
    e.preventDefault();
    $(".aviso-cookies").show();
  })

  $(".aviso-cookies").show();

  $(".aceitar-cookies").click(function () {
    var url = window.location.origin + "/previa-tecaa/aceite-de-cookies";

    $.ajax({
      type: "POST",
      url: url,
      success: function (data, textStatus, jqXHR) {
        $(".aviso-cookies").hide();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  });
});
