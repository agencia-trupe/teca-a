const config = {
  padrao: {
    toolbar: [["Bold", "Italic"], ["RemoveFormat"]],
  },

  clean: {
    toolbar: [],
    removePlugins: "toolbar,elementspath",
  },

  textBlog: {
    toolbar: [
      ["Bold", "Italic"],
      ["Format"],
      ["BulletedList", "NumberedList"],
      ["Link", "Unlink"],
      ["Table"],
      ["InjectImage"],
      ["RemoveFormat"],
    ],
  },
};

export default function TextEditor() {
  CKEDITOR.config.language = "pt-br";
  CKEDITOR.config.uiColor = "#dce4ec";
  CKEDITOR.config.contentsCss = [
    $("base").attr("href") + "/assets/ckeditor.css",
    CKEDITOR.config.contentsCss,
  ];
  CKEDITOR.config.removePlugins = "elementspath";
  CKEDITOR.config.resize_enabled = false;
  CKEDITOR.plugins.addExternal(
    "injectimage",
    $("base").attr("href") + "/assets/injectimage/plugin.js"
  );
  CKEDITOR.config.allowedContent = true;
  CKEDITOR.config.extraPlugins = "injectimage,colorbutton,removeformat,table";

  $(".ckeditor").each(function (i, obj) {
    CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
  });
}
