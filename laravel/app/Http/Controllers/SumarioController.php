<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Cadastro;
use App\Models\Capitulo;
use App\Models\CapituloPosts;
use App\Models\Status;

class SumarioController extends Controller
{
    public function index()
    {
        $cadastro = auth('cadastros')->user();

        $capitulos = Capitulo::ordenados()->get();

        $posts = CapituloPosts::ordenados()->get();

        $status = Status::where('cadastro_id', $cadastro->id)->get();

        return view('frontend.sumario', compact('capitulos', 'posts', 'status'));
    }

    public function showPost($capitulo, $post, $proximo = null)
    {
        $cadastro = auth('cadastros')->user();
        $capituloAtual = Capitulo::where('slug', $capitulo)->first();
        $postAtual = CapituloPosts::where('capitulo_id', $capituloAtual->id)->where('slug', $post)->first();

        if (empty($proximo)) {
            $postTexto = CapituloPosts::join('capitulos', 'capitulos.id', '=', 'capitulos_posts.capitulo_id')
                ->where('capitulos_posts.slug', $post)
                ->where('capitulos.slug', $capitulo)
                ->select('capitulos.slug as cap_slug', 'capitulos.titulo as cap_titulo', 'capitulos.cor as cor', 'capitulos_posts.*')
                ->first();
        } else {
            $postTexto = CapituloPosts::join('capitulos', 'capitulos.id', '=', 'capitulos_posts.capitulo_id')
                ->where('capitulos_posts.capitulo_id', $capituloAtual->id)
                ->select('capitulos.slug as cap_slug', 'capitulos.titulo as cap_titulo', 'capitulos.cor as cor', 'capitulos_posts.*')
                ->ordenados()->where('capitulos_posts.ordem', '>', $postAtual->ordem)->first();
            if (empty($postTexto)) {
                $capituloNext =  Capitulo::where('ordem', '>', $capituloAtual->ordem)->first();
                $postTexto = CapituloPosts::join('capitulos', 'capitulos.id', '=', 'capitulos_posts.capitulo_id')
                    ->where('capitulos_posts.capitulo_id', $capituloNext->id)
                    ->select('capitulos.slug as cap_slug', 'capitulos.titulo as cap_titulo', 'capitulos.cor as cor', 'capitulos_posts.*')
                    ->ordenados()->first();
            }
        }

        $status = Status::where('cadastro_id', $cadastro->id)->where('post_id', $postTexto->id)->first();

        if (isset($status)) {
            $status->update([
                'cadastro_id' => $cadastro->id,
                'post_id'     => $postTexto->id,
                'iniciado'    => 1,
            ]);
        } else {
            $status = Status::create([
                'cadastro_id' => $cadastro->id,
                'post_id'     => $postTexto->id,
                'iniciado'    => 1,
            ]);
        }

        return view('frontend.paginas', compact('postTexto', 'status'));
    }

    public function postConcluido($cadastro, $post)
    {
        $cadastro = Cadastro::where('id', $cadastro)->first();
        $post = CapituloPosts::where('id', $post)->first();

        $status = Status::where('cadastro_id', $cadastro->id)->where('post_id', $post->id)->first();

        if (isset($status)) {
            $status->update([
                'cadastro_id' => $cadastro->id,
                'post_id'     => $post->id,
                'iniciado'    => 1,
                'concluido'   => 1,
            ]);
        } else {
            $status = Status::create([
                'cadastro_id' => $cadastro->id,
                'post_id'     => $post->id,
                'iniciado'    => 1,
                'concluido'   => 1,
            ]);
        }

        return response()->json(['cadastro' => $cadastro, 'post' => $post, 'status' => $status]);
    }
}
