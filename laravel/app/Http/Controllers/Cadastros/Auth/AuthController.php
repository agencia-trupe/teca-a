<?php

namespace App\Http\Controllers\Cadastros\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CadastrosRequest;
use App\Models\Cadastro;
use App\Models\Email;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected function showLoginForm()
    {
        auth('cadastros')->logout();
        return view('frontend.cadastros-login');
    }

    protected function login(Request $request)
    {
        if (Auth::guard('cadastros')->attempt([
            'email'    => $request->get('email'),
            'password' => $request->get('senha')
        ])) {
            $emailsLiberados = Email::get();

            if (count($emailsLiberados->where('email', $request->email)) != 0) {
                return redirect()->route('sumario')->with('success', 'Usuário logado!');
            } else {
                return redirect()->route('conteudo-restrito');
            }
        } else {
            return redirect()->route('cadastros.login')->with('error', 'Erro ao logar, e-mail ou senha inválidos.');
        }
    }

    protected function showCreateForm()
    {
        return view('frontend.cadastros-create');
    }

    protected function create(CadastrosRequest $request)
    {
        try {
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $emailsLiberados = Email::get();

            if (count($emailsLiberados->where('email', $request->email)) != 0) {
                $input['liberado'] = 1;

                $cadastro = Cadastro::create($input);

                Auth::guard('cadastros')->loginUsingId($cadastro->id);

                return redirect()->route('sumario');
            } else {
                return redirect()->route('conteudo-restrito');
            }
        } catch (\Exception $e) {

            return back()->withErrors(['Erro: ' . $e->getMessage()]);
        }
    }

    protected function showUpdateForm($id)
    {
        $user = Cadastro::where('id', $id)->first();

        return view('frontend.cadastros-update', compact('user'));
    }

    public function update(CadastrosRequest $request, $id)
    {
        try {
            $input = $request->all();
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $user = Cadastro::where('id', $id)->first();
            $user->update($input);

            return redirect()->route('sumario');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro: ' . $e->getMessage()]);
        }
    }

    protected function logout()
    {
        auth('cadastros')->logout();

        return redirect()->route('cadastros.login');
    }
}
