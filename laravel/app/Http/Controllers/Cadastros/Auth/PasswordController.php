<?php

namespace App\Http\Controllers\Cadastros\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $subject = 'Recuperação de senha';

    protected $broker          = 'cadastros';

    public function showResetRequestForm()
    {
        return view('frontend.cadastros-reset-request');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ], [
            'email.required' => 'insira um endereço de e-mail válido',
            'email.email'    => 'insira um endereço de e-mail válido',
        ]);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('success', 'Um e-mail foi enviado com instruções para a redefinição de senha.');

            case Password::INVALID_USER:
            default:
                return redirect()->back()->with('error', 'E-mail não encontrado.');
        }
    }

    protected function getSendResetLinkEmailSuccessResponse()
    {
        return redirect()->route('login')->with([
            'senhaRedefinida' => request('email')
        ]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->input('email');

        return view('frontend.cadastros-reset-password', compact('email', 'token'));
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules(), [
            'password.required'  => 'insira uma senha',
            'password.min'       => 'sua senha deve ter no mínimo 6 caracteres',
            'password.confirmed' => 'a confirmação de senha não confere',
        ]);

        $credentials = $request->only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);

            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->senha = bcrypt($password);

        $user->save();

        Auth::guard($this->getGuard())->login($user);
    }

    protected function getResetSuccessResponse($response)
    {
        return redirect()->route('cadastros.login')->with('success', 'Senha redefinida com sucesso.');
    }
}
