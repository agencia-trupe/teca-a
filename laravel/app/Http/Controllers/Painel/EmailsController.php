<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailsRequest;
use App\Models\Email;

class EmailsController extends Controller
{
    public function index()
    {
        $emails = Email::orderBy('email', 'asc')->get();

        return view('painel.emails.index', compact('emails'));
    }

    public function create()
    {
        return view('painel.emails.create');
    }

    public function store(EmailsRequest $request)
    {
        try {
            $input = $request->all();

            Email::create($input);

            return redirect()->route('painel.emails.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Email $email)
    {
        return view('painel.emails.edit', compact('email'));
    }

    public function update(EmailsRequest $request, Email $email)
    {
        try {
            $input = $request->all();

            $email->update($input);

            return redirect()->route('painel.emails.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Email $email)
    {
        try {
            $email->delete();

            return redirect()->route('painel.emails.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
