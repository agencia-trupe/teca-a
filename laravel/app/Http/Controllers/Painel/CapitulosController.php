<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CapitulosRequest;
use App\Models\Capitulo;

class CapitulosController extends Controller
{
    public function index()
    {
        $capitulos = Capitulo::ordenados()->get();

        return view('painel.capitulos.index', compact('capitulos'));
    }

    public function create()
    {
        return view('painel.capitulos.create');
    }

    public function store(CapitulosRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            Capitulo::create($input);

            return redirect()->route('painel.capitulos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Capitulo $capitulo)
    {
        return view('painel.capitulos.edit', compact('capitulo'));
    }

    public function update(CapitulosRequest $request, Capitulo $capitulo)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $capitulo->update($input);

            return redirect()->route('painel.capitulos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Capitulo $capitulo)
    {
        try {
            $capitulo->delete();

            return redirect()->route('painel.capitulos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
