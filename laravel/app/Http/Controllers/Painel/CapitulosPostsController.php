<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CapitulosPostsRequest;
use App\Models\Capitulo;
use App\Models\CapituloPosts;

class CapitulosPostsController extends Controller
{
    public function index(Capitulo $capitulo)
    {
        $posts = CapituloPosts::capitulo($capitulo->id)->ordenados()->get();

        return view('painel.capitulos.posts.index', compact('capitulo', 'posts'));
    }

    public function create(Capitulo $capitulo)
    {
        return view('painel.capitulos.posts.create', compact('capitulo'));
    }

    public function store(CapitulosPostsRequest $request, Capitulo $capitulo)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo_post, "-");
            $input['capitulo_id'] = $capitulo->id;

            CapituloPosts::create($input);

            return redirect()->route('painel.capitulos.posts.index', $capitulo->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Capitulo $capitulo, CapituloPosts $posts)
    {
        return view('painel.capitulos.posts.edit', compact('capitulo', 'posts'));
    }

    public function update(CapitulosPostsRequest $request, Capitulo $capitulo, CapituloPosts $posts)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo_post, "-");
            $input['capitulo_id'] = $capitulo->id;

            $posts->update($input);

            return redirect()->route('painel.capitulos.posts.index', $capitulo->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Capitulo $capitulo, CapituloPosts $posts)
    {
        try {
            $posts->delete();

            return redirect()->route('painel.capitulos.posts.index', $capitulo->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
