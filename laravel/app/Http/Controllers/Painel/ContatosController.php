<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatosRequest;
use App\Models\Contato;

class ContatosController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('painel.contatos.edit', compact('contato'));
    }

    public function update(ContatosRequest $request, Contato $contato)
    {
        try {
            $input = $request->all();

            $contato->update($input);

            return redirect()->route('painel.contatos.index')->with('success', 'Informações alteradas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar informações: ' . $e->getMessage()]);
        }
    }
}
