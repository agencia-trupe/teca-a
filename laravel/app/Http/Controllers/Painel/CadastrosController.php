<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cadastro;

class CadastrosController extends Controller
{
    public function index()
    {
        $cadastros = Cadastro::liberado()->orderBy('nome', 'asc')->get();

        return view('painel.cadastros.index', compact('cadastros'));
    }
}
