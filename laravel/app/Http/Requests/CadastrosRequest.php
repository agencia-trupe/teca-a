<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastrosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'       => 'required',
            'email'      => 'required|email',
            'senha'      => 'required|min:8|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => 'preencha seu nome',
            'email.required'    => 'preencha seu e-mail',
            'email.email'       => 'insira um endereço de e-mail válido',
            'senha.required'    => 'insira uma senha',
            'senha.confirmed'   => 'a confirmação de senha não confere',
            'senha.min'         => 'sua senha deve ter no mínimo 8 caracteres',
        ];
    }
}
