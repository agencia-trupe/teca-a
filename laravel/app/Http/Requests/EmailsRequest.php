<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmailsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'    => 'required|email|max:255|unique:emails',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'email'    => 'Insira um e-mail válido.',
            'unique'   => 'Esse e-mail já foi cadastrado.'
        ]; 
    }
}
