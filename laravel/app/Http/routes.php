<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        if (auth('cadastros')->check()) {
            return redirect()->route('sumario');
        } else {
            return redirect()->route('cadastros.login');
        }
    });
    Route::get('cadastros/login', 'Cadastros\Auth\AuthController@showLoginForm')->name('cadastros.login');
    Route::get('cadastros/esqueci-minha-senha', 'Cadastros\Auth\PasswordController@showResetRequestForm')->name('cadastros.reset-request');
    Route::get('cadastros/redefinir-senha/{token}', 'Cadastros\Auth\PasswordController@showResetForm')->name('cadastros.reset');
    Route::get('cadastros/novo-cadastro', 'Cadastros\Auth\AuthController@showCreateForm')->name('cadastros.create');
    Route::get('cadastros/alterar-cadastro/{user}', 'Cadastros\Auth\AuthController@showUpdateForm')->name('cadastros.update');
    Route::get('conteudo-restrito', function () {
        return view('frontend.conteudo-restrito');
    })->name('conteudo-restrito');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'CookiesController@postCookies')->name('aceite-de-cookies.post');

    // Cadastros LOGADOS
    Route::group(['middleware' => 'cadastros.auth'], function () {
        Route::get('sumario', 'SumarioController@index')->name('sumario');
        Route::get('capitulo/{capitulo}/{post}/{proximo?}', 'SumarioController@showPost')->name('post');
        Route::get('{cadastro}/{post}/concluido', 'SumarioController@postConcluido')->name('post.concluido');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::resource('usuarios', 'UsuariosController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::resource('contatos', 'ContatosController');
        Route::resource('capitulos', 'CapitulosController');
        Route::resource('capitulos.posts', 'CapitulosPostsController');
        Route::resource('emails', 'EmailsController');
        Route::get('cadastros', 'CadastrosController@index')->name('painel.cadastros.index');

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('cache:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('view:clear');
            $exitCode = Artisan::call('config:cache');

            return 'DONE';
        });
    });

    // Auth PAINEL
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });

    // Auth CADASTROS
    Route::group([
        'prefix' => 'cadastros',
        'namespace' => 'Cadastros',
    ], function () {
        Route::post('login', 'Auth\AuthController@login')->name('cadastros.login');
        Route::post('cadastro', 'Auth\AuthController@create')->name('register.post');
        Route::post('cadastro/{user}', 'Auth\AuthController@update')->name('update.post');
        Route::post('esqueci-minha-senha', 'Auth\PasswordController@sendResetLinkEmail')->name('reset-request.post');
        Route::post('redefinicao-de-senha', 'Auth\PasswordController@reset')->name('reset.post');
        Route::get('logout', 'Auth\AuthController@logout')->name('cadastros.logout');
    });
});
