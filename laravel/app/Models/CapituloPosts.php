<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CapituloPosts extends Model
{
    protected $table = 'capitulos_posts';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCapitulo($query, $id)
    {
        return $query->where('capitulo_id', $id);
    }
}
