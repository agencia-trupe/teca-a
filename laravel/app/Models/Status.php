<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    protected $guarded = ['id'];

    public function scopeCadastro($query, $id)
    {
        return $query->where('cadastro_id', $id);
    }

    public function scopePost($query, $id)
    {
        return $query->where('post_id', $id);
    }

    public function scopeIniciado($query)
    {
        return $query->where('iniciado', '=', 1);
    }

    public function scopeConcluido($query)
    {
        return $query->where('concluido', '=', 1);
    }
}
